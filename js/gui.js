var fullscreenCanvas = null;
var showAsMinimal = false;
var keyZones = [
    ["right", [39]],
    ["left", [37]],
    ["up", [38]],
    ["down", [40]],
    ["a", [88, 74]],
    ["b", [90, 81, 89]],
    ["select", [16]],
    ["start", [13]]
];

function readFile(input){
if (typeof input.files != "undefined" &&  input.files.length >= 1) {
        console.log("Reading the local file \"" + input.files[0].name + "\"", 0);         
        var ext = input.value.substr( (input.value.lastIndexOf('.') +1) );    
        switch (ext) {
          case 'gb':
          case 'gbc':
            break; //do nothing          
          default:
            alert ("Only '.gb' and '.gbc' files are allowed");
            return;
        }
        var binaryHandle = new FileReader(); 
        binaryHandle.onloadstart = function (){
          $.mobile.loading( 'show', {
            text: 'Loading file ...',
            textVisible: true,                                          
          });
          console.log("mobile loading");
        };                    
        binaryHandle.onloadend = function (){
          if (binaryHandle.error != null){
            console.log(error.message + " file: " + error.fileName + " line: " + error.lineNumber);
            alert("Error while loading file");
            $.mobile.loading('hide');         
            return;   
          }
          console.log("file loaded.", 0);
          try {                                                                       
            start(fullscreenCanvas, this.result);
          }
          catch (error) {
            console.log(error.message + " file: " + error.fileName + " line: " + error.lineNumber);
            alert("Error while loading file");            
          }finally{
            $.mobile.loading('hide');
          }
        };
        binaryHandle.readAsBinaryString(input.files[0]);                 
      }
      else {
        alert("You must select only one file.");
      }	
}

function windowingInitialize() {
    //TODO REMOVE JQUERY 
    var navBarHeight = $("#nav-bar").height() + 12;//12 pixel for pading    
    $(".actions").css("bottom",navBarHeight + "px"); 
    var canvas = document.getElementById("fullscreen"); 
    canvas.style.width = $(document).width() + 'px'; 
    canvas.style.height = $(document).height() - navBarHeight + 'px'; 
    fullscreenCanvas = document.getElementById("fullscreen");
    registerGUIEvents();
}
function registerGUIEvents() {
    addEvent("keydown", document, keyDown);
    addEvent("keyup", document,  function (event) {
            keyUp(event);
    });
    addEvent("MozOrientation", window, GameBoyGyroSignalHandler);
    addEvent("deviceorientation", window, GameBoyGyroSignalHandler);   
    addEvent("click", document.getElementById("set_volume"), function () {
        if (GameBoyEmulatorInitialized()) {
            var volume = prompt("Set the volume here:", "1.0");
            if (volume != null && volume.length > 0) {
                settings[3] = Math.min(Math.max(parseFloat(volume), 0), 1);
                gameboy.changeVolume();
            }
        }
        jQuery(".actions").hide();
    });
    addEvent("click", document.getElementById("set_speed"), function () {
        jQuery(".actions").hide();
        if (GameBoyEmulatorInitialized()) {
            var speed = prompt("Set the emulator speed here:", "1.0");
            if (speed != null && speed.length > 0) {
                gameboy.setSpeed(Math.max(parseFloat(speed), 0.001));
            }
        }       
    });
  $("#openFile").click(function (){	  
    $('<input/>').attr('type', 'file').css('display','none').change(function (){
		readFile(this);		      
    }).click(); // end change function, fire event
  }); // end of click function
    addEvent("click", document.getElementById("restart_cpu_clicker"), function () {
        jQuery(".actions").hide();
        if (GameBoyEmulatorInitialized()) {
            try {
                if (!gameboy.fromSaveState) {
                    
                    start(fullscreenCanvas, gameboy.getROMImage());
                }
                else {
                    
                    openState(gameboy.savedStateFileName, fullscreenCanvas);
                }
            }
            catch (error) {
                console.log(error.message + " file: " + error.fileName + " line: " + error.lineNumber);
            }
        }
        else {
            console.log("Could not restart, as a previous emulation session could not be found.", 1);
        }       
    });
    addEvent("click", document.getElementById("run_cpu_clicker"), function () {
        jQuery(".actions").hide();
        run();      
    });
    addEvent("click", document.getElementById("kill_cpu_clicker"), function () {
        jQuery(".actions").hide();
        pause();        
    });
    addEvent("click", document.getElementById("save_state_clicker"), function () {
        jQuery(".actions").hide();
        jQuery("#save_menu").show();        
    });
    addEvent("click", document.getElementById("load_state_clicker"), function () {
        jQuery(".actions").hide();
        jQuery("#load_menu").show();        
    }); 
    $(".saveMenuItem").click(function (){
        jQuery(".actions").hide();
        save($(this).text());       
    });             
    $(".loadMenuItem").click(function (){
        jQuery(".actions").hide();
        runFreeze($(this).text());      
    }); 		
    addEvent("resize", window, initNewCanvasSize);
    
}
function keyDown(event) {
    var keyCode = event.keyCode;
    var keyMapLength = keyZones.length;
    for (var keyMapIndex = 0; keyMapIndex < keyMapLength; ++keyMapIndex) {
        var keyCheck = keyZones[keyMapIndex];
        var keysMapped = keyCheck[1];
        var keysTotal = keysMapped.length;
        for (var index = 0; index < keysTotal; ++index) {
            if (keysMapped[index] == keyCode) {
                GameBoyKeyDown(keyCheck[0]);
                try {
                    event.preventDefault();
                }
                catch (error) { }
            }
        }
    }
}
function keyUp(event) { 
    var keyCode = event.keyCode;
    var keyMapLength = keyZones.length;
    for (var keyMapIndex = 0; keyMapIndex < keyMapLength; ++keyMapIndex) {
        var keyCheck = keyZones[keyMapIndex];
        var keysMapped = keyCheck[1];
        var keysTotal = keysMapped.length;
        for (var index = 0; index < keysTotal; ++index) {
            if (keysMapped[index] == keyCode) {
                GameBoyKeyUp(keyCheck[0]);
                try {
                    event.preventDefault();
                }
                catch (error) { }
            }
        }
    }
}

function runFreeze(slot) {
    try {
        var name = gameboy.name + "_" + slot;
        var store = getObjectStore(DB_STORE_NAME, 'readonly');
        var req = store.get(name);
        req.onsuccess = function (event){
            openState(event, fullscreenCanvas);
        };
        req.onerror = function (event){
            console.log(error);
        };
    }
    catch (error) {
        console.log("A problem with attempting to open the selected save state occurred.", 2);
    }
}

function addEvent(sEvent, oElement, fListener) {
    oElement.addEventListener(sEvent, fListener, false);
}
