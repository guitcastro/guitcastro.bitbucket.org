const DB_NAME = 'gameboy';
const DB_VERSION = 3; // Use a long long for this value (don't use a float)
const DB_STORE_NAME = 'saves';
var db;
var saveCount = -1;

// Used to keep track of which view is displayed to avoid to uselessly reload it
var current_view_pub_key;

function openDb() {
	console.log("openDb ...");
	var req = indexedDB.open(DB_NAME, DB_VERSION);
	req.onsuccess = function (evt) {
		// Better use "this" than "req" to get the result to avoid problems with
		// garbage collection.
		// db = req.result;
		db = this.result;
		getSavedGamesCount();
		console.log("openDb DONE");
	};
	req.onerror = function (evt) {
		console.error("openDb:", evt.target.errorCode);
};

req.onupgradeneeded = function (evt) {
  console.log("openDb.onupgradeneeded");
  var store = evt.currentTarget.result.createObjectStore(DB_STORE_NAME, { keyPath: 'name'});
};
}

function addSaveGame(name, blob) {
   console.log("add Save arguments:", arguments);
   var obj = { "name": name, "blob": blob };
   if (typeof blob != 'undefined')
      obj.blob = blob;
   var store = getObjectStore(DB_STORE_NAME, 'readwrite');
   var req;
   try {
     req = store.put(obj);
   } catch (e) {
      console.log(e);
   }
   req.onsuccess = function (evt) {
      console.log("Insertion in DB successful");
   };
   req.onerror = function() {
     console.error("error", this.error);      
   };
}

/**
* @param {string} store_name
* @param {string} mode either "readonly" or "readwrite"
*/
function getObjectStore(store_name, mode) {
var tx = db.transaction(store_name, mode);
return tx.objectStore(store_name);
}

function getSavedGamesCount(){
var store = getObjectStore(DB_STORE_NAME, 'readonly');
var req = store.count();
req.onsuccess = function (e){ 
	saveCount = e.target.result;
};

}
 
window.addEventListener("DOMContentLoaded", openDb, false);   
