var VirtualJoystick	= function(opts)
{
	opts			= opts			|| {};
	this._container		= opts.container	|| document.body;
	this._mouseSupport	= 'mouseSupport' in opts? opts.mouseSupport	: false;
	this._range		= opts.range		|| 60;

    var dpad =  document.createElement("img");
    dpad.setAttribute("src","images/dpad2.png");
    var pad =  document.createElement("img");
    pad.setAttribute("src","images/pad2.png");

    this._stickEl = pad;	
	this._baseEl  = dpad;

	this._container.style.position	= "relative";

	this._container.appendChild(this._baseEl);
	this._baseEl.style.position	= "absolute"
	this._baseEl.style.display	= "none";
	
	this._container.appendChild(this._stickEl);
	this._stickEl.style.position	= "absolute"
	this._stickEl.style.display	= "none";
	
	var button =  $("<img>");	
	button.css("position","absolute");
	button.css("z-index","1");
	button.css("opacity","0.2");
	
	var buttonB = button.clone();	
    buttonB.mousedown(function(){
        trigger("keyup",90);
    });	
    buttonB.mouseup(function(){
        trigger("keydown",90);
    });	
	buttonB.css("bottom","60px");
	buttonB.css("right","90px");
	buttonB.css("display","block");
	buttonB.attr("src","images/bbutton.png");
	$('body').append(buttonB);
	
	var buttonA = button.clone();	
	buttonA.mousedown(function() {
		trigger("keyup",88);
	});
    buttonA.mouseup(function(){
        trigger("keydown",88);
    });	
	buttonA.css("bottom","100px");
	buttonA.css("right","15px");
	buttonA.css("display","block");	
	buttonA.attr("src","images/abutton.png");
	$('body').append(buttonA);	
	
	this._pressed	= false;
	this.isRightPressed = false;
	this.isLeftPressed = false;
	this.isUpPressed = false;
	this.isDownPressed = false;
	this._baseX	= 0;
	this._baseY	= 0;
	this._stickX	= 0;
	this._stickY	= 0;

	__bind		= function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
	this._$onTouchStart	= __bind(this._onTouchStart	, this);
	this._$onTouchEnd	= __bind(this._onTouchEnd	, this);
	this._$onTouchMove	= __bind(this._onTouchMove	, this);
	this._container.addEventListener( 'touchstart'	, this._$onTouchStart	, false );
	this._container.addEventListener( 'touchend'	, this._$onTouchEnd	, false );
	this._container.addEventListener( 'touchmove'	, this._$onTouchMove	, false );	
	if( this._mouseSupport ){
		this._$onMouseDown	= __bind(this._onMouseDown	, this);
		this._$onMouseUp	= __bind(this._onMouseUp	, this);
		this._$onMouseMove	= __bind(this._onMouseMove	, this);
		this._container.addEventListener( 'mousedown'	, this._$onMouseDown	, false );
		this._container.addEventListener( 'mouseup'	, this._$onMouseUp	, false );
		this._container.addEventListener( 'mousemove'	, this._$onMouseMove	, false );
	}	
}

VirtualJoystick.prototype.destroy	= function()
{
	this._container.removeChild(this._baseEl);
	this._container.removeChild(this._stickEl);

	this._container.removeEventListener( 'touchstart'	, this._$onTouchStart	, false );
	this._container.removeEventListener( 'touchend'		, this._$onTouchEnd	, false );
	this._container.removeEventListener( 'touchmove'	, this._$onTouchMove	, false );
	if( this._mouseSupport ){
		this._container.removeEventListener( 'mouseup'		, this._$onMouseUp	, false );
		this._container.removeEventListener( 'mousedown'	, this._$onMouseDown	, false );
		this._container.removeEventListener( 'mousemove'	, this._$onMouseMove	, false );
	}
}

/**
 * @returns {Boolean} true if touchscreen is currently available, false otherwise
*/
VirtualJoystick.touchScreenAvailable	= function()
{
	return 'createTouch' in document ? true : false;
}

//////////////////////////////////////////////////////////////////////////////////
//										//
//////////////////////////////////////////////////////////////////////////////////

VirtualJoystick.prototype.deltaX	= function(){ return this._stickX - this._baseX;	}
VirtualJoystick.prototype.deltaY	= function(){ return this._stickY - this._baseY;	}

VirtualJoystick.prototype.up	= function(){
	if( this._pressed === false )	return false;
	var deltaX	= this.deltaX();
	var deltaY	= this.deltaY();
	if( deltaY >= 0 )	return false;
	if( Math.abs(deltaY) < this._range && Math.abs(deltaY) < Math.abs(deltaX) ){
		return false;
	}
	return true;
}
VirtualJoystick.prototype.down	= function(){
	if( this._pressed === false )	return false;
	var deltaX	= this.deltaX();
	var deltaY	= this.deltaY();
	if( deltaY <= 0 )	return false;
	if( Math.abs(deltaY) < this._range && Math.abs(deltaY) < Math.abs(deltaX) ){
		return false;
	}
	return true;	
}
VirtualJoystick.prototype.right	= function(){
	if( this._pressed === false )	return false;
	var deltaX	= this.deltaX();
	var deltaY	= this.deltaY();
	if( deltaX <= 0 )	return false;
	if( Math.abs(deltaX) < this._range && Math.abs(deltaY) > Math.abs(deltaX) ){
		return false;
	}
	return true;	
}
VirtualJoystick.prototype.left	= function(){
	if( this._pressed === false )	return false;
	var deltaX	= this.deltaX();
	var deltaY	= this.deltaY();
	if( deltaX >= 0 )	return false;
	if( Math.abs(deltaX) < this._range && Math.abs(deltaY) > Math.abs(deltaX) ){
		return false;
	}
	return true;	
}

//////////////////////////////////////////////////////////////////////////////////
//										//
//////////////////////////////////////////////////////////////////////////////////

VirtualJoystick.prototype._onUp	= function()
{
	if (this.isUpPressed){		
		this._trigger("keyup",38);
	}							
	if (this.isRightPressed){
		this._trigger("keyup",39);
	}			
	if (this.isLeftPressed){
		this._trigger("keyup",37);
	}		
	if (this.isDownPressed){
		this._trigger("keyup",40);
	}					
	
	this.isRightPressed = false;
	this.isLeftPressed = false;
	this.isUpPressed = false;
	this.isDownPressed = false;

	this._pressed	= false; 
	this._stickEl.style.display	= "none";
	this._baseEl.style.display	= "none";
	
	this._baseX	= this._baseY	= 0;
	this._stickX	= this._stickY	= 0;
}

VirtualJoystick.prototype._onDown	= function(x, y)
{
	this._pressed	= true; 
	this._baseX	= x;
	this._baseY	= y;
	this._stickX	= x;
	this._stickY	= y;

	this._stickEl.style.display	= "";
	this._stickEl.style.left	= (x - this._stickEl.width /2)+"px";
	this._stickEl.style.top		= (y - this._stickEl.height/2)+"px";

	this._baseEl.style.display	= "";
	this._baseEl.style.left		= (x - this._baseEl.width /2)+"px";
	this._baseEl.style.top		= (y - this._baseEl.height/2)+"px";
}

VirtualJoystick.prototype._trigger = function(event,keyCode){
			
	var keyboardEvent = document.createEvent("KeyboardEvent");
	var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";
	keyboardEvent[initMethod](
        event, // event type : keydown, keyup, keypress
        true, // bubbles
        true, // cancelable
        window, // viewArg: should be window
        false, // ctrlKeyArg
        false, // altKeyArg
        false, // shiftKeyArg
        false, // metaKeyArg
        keyCode, // keyCodeArg : unsigned long the virtual key code, else 0
        0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
	);
	document.dispatchEvent(keyboardEvent);
}

VirtualJoystick.prototype.triggerStart = function(event,keyCode){
	console.log("button start");
	this._trigger("keydown",13);
	this._trigger("keyup",13);
}

VirtualJoystick.prototype.triggerSelect = function(event,keyCode){
	this._trigger("keydown",16);		
	this._trigger("keyup",16);
}

VirtualJoystick.prototype._onMove	= function(x, y)
{
	if( this._pressed === true ){
		if (Math.abs(this._baseX - x) > 20){
			if (this._baseX > x){
				x = this._baseX - 20;
			}else{
				x = this._baseX + 20;
			}
		}
		if (Math.abs(this._baseY - y) > 20){
			if (this._baseY > y){
				y = this._baseY - 20;
			}else{
				y = this._baseY + 20;
			}
		}		
		this._stickX	= x;
		this._stickY	= y;

		if (this.isUpPressed && !this.up()){
			this.isUpPressed = false;				
			this._trigger("keyup",38);
		}							
		else if (!this.isUpPressed && this.up()){
			this.isUpPressed = true;
			this._trigger("keydown",38);
		}

		if (this.isRightPressed && !this.right()){
			this.isRightPressed = false;
			this._trigger("keyup",39);
		}			
		else if (!this.isRightPressed && this.right()){
			this.isRightPressed = true;
			this._trigger("keydown",39);
		}

		if (this.isLeftPressed && !this.left()){
			this.isLeftPressed = false;
			this._trigger("keyup",37);
		}		
		else if (!this.isLeftPressed && this.left()){			
			this.isLeftPressed = true;
			this._trigger("keydown",37);
		}

		if (this.isDownPressed && !this.down()){
			this.isDownPressed = false;
			this._trigger("keyup",40);
		}					
		else if (!this.isDownPressed && this.down()){
			this.isDownPressed = true;
			this._trigger("keydown",40);
		}	
		this._stickEl.style.left = (x - this._stickEl.width /2)+"px";
		this._stickEl.style.top		= (y - this._stickEl.height/2)+"px";
	}
}


//////////////////////////////////////////////////////////////////////////////////
//		bind touch events (and mouse events for debug)			//
//////////////////////////////////////////////////////////////////////////////////

VirtualJoystick.prototype._onMouseUp	= function(event)
{
	return this._onUp();
}

VirtualJoystick.prototype._onMouseDown	= function(event)
{
	var x	= event.clientX;
	var y	= event.clientY;
	return this._onDown(x, y);
}

VirtualJoystick.prototype._onMouseMove	= function(event)
{
	var x	= event.clientX;
	var y	= event.clientY;
	return this._onMove(x, y);
}

VirtualJoystick.prototype._onTouchStart	= function(event)
{
	if( event.touches.length != 1 )	return;

	event.preventDefault();

	var x	= event.touches[ 0 ].pageX;
	var y	= event.touches[ 0 ].pageY;
	return this._onDown(x, y)
}

VirtualJoystick.prototype._onTouchEnd	= function(event)
{
	
// no preventDefault to get click event on ios
    event.preventDefault();
	return this._onUp()
}

VirtualJoystick.prototype._onTouchMove	= function(event)
{
	if( event.touches.length != 1 )	return;

	event.preventDefault();

	var x	= event.touches[ 0 ].pageX;
	var y	= event.touches[ 0 ].pageY;
	return this._onMove(x, y)
}

VirtualJoystick.prototype._buildButton	= function(text,x,y)
{
	var canvas	= document.createElement( 'canvas' );
	canvas.width	= 56;
	canvas.height	= 56;
	var ctx	= canvas.getContext('2d');
	ctx.beginPath();
	ctx.strokeStyle	= "rgba(255,0,0, 0.3)";;
	ctx.fillStyle = "rgba(255,0,0, 0.5)";
	ctx.globalAlpha = 0.5
	ctx.lineWidth	= 3;
	ctx.font = "20pt Arial";
	ctx.arc(canvas.width/2, canvas.width/2, 25, 0, Math.PI*2, true);	
	ctx.textAlign = "center";
	ctx.textBaseline = "middle";	
	ctx.fillText(text,canvas.width/2,canvas.width/2);
	ctx.stroke();	
	return canvas;
}

trigger = function(event,keyCode){
			
	var keyboardEvent = document.createEvent("KeyboardEvent");
	var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";
	keyboardEvent[initMethod](
        event, // event type : keydown, keyup, keypress
        true, // bubbles
        true, // cancelable
        window, // viewArg: should be window
        false, // ctrlKeyArg
        false, // altKeyArg
        false, // shiftKeyArg
        false, // metaKeyArg
        keyCode, // keyCodeArg : unsigned long the virtual key code, else 0
        0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
	);
	document.dispatchEvent(keyboardEvent);
};

		
		setInterval(function(){						
			
			
		}, 100);
